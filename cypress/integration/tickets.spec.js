describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("fills all text input fields", () =>{
        const firstName = "Enoque";
        const lastName = "Fernandes";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("netto@example.com");
        cy.get("#requests").type("Vegetarian");
        cy.get("#signature").type(`${firstName} ${lastName}`);

    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });

    it("select 'social media' checkbox", () =>{
        cy.get("#social-media").check();
    });

    it("selcect 'friend', and 'publication', the uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("invalid 'email' verification", () => {
        cy.get("#email")
         .as("email")
         .type("netto18-example.com");

         cy.get("#email.invalid").should("exist");

        cy.get("@email")
         .clear()
         .type("netto@example.com");

         cy.get("#email.invalid").should("not.exist");
    })

    it("fill and reset the form", () => {
        const firstName = "Enoque";
        const lastName = "Fernandes";
        const fullName = `${firstName} ${lastName}`

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("netto@example.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").click();
        cy.get("#social-media").click();
        cy.get("#requests").type("vegetarian");
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("button[type='submit']")
         .as("submitButton")
         .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "Enoque",
            lastName: "Fernandes",
            email: "netto@example.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
         .as("subimitButton")
         .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@subimitButton").should("be.disabled");
    });

});